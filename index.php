<?php
	require 'animal.php';
	require 'ape.php'; 
	require 'frog.php';
	$sheep = new Animal("shaun");

	echo $sheep->name . "<br>"; // "shaun"
	echo $sheep->legs . "<br>"; // 2
	echo var_dump($sheep->cold_blooded) . "<br><br>"; // false

	$sungokong = new Ape("kera sakti");
	echo $sungokong->name . "<br>";
	$sungokong->yell(); // "Auooo"

	$kodok = new Frog("buduk");
	echo $kodok->name . "<br>";
	$kodok->jump() ; // "hop hop"


?>